package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import models._
import play.api.libs.json._
import play.api.libs.json.Reads._


@Singleton
class SurveyController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  /**
   * Create an Action to render Json for all Survey Resources.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def getSurveys() = Action { implicit request: Request[AnyContent] =>
    val result = Survey.fetch
    if (result.isEmpty){
      NoContent
    }else{
      Ok(Json.toJson(result))
    }

//    Survey.fetch match {
//    case head :: tail => Ok(Json.toJson(head :: tail))
//    case _ => NoContent
//    }
  }

  /**
    * Create an Action to render Json for Surveys with a particular gender
    *
    */
  def getSurveysByGender(gender: String) = Action { implicit request: Request[AnyContent] =>
    val list = Survey.fetchByGender(gender)
    Ok(Json.toJson(list))


  }

  /**
    * Create an Action to render Json for Surveys with a particular age
    *
    */
  def getSurveysByAge(age: Int) = Action { implicit request: Request[AnyContent] =>
    val list = Survey.fetchByAge(age)
    Ok(Json.toJson(list))


  }
}
