
package models
import scala.util._
import play.api.libs.json._
import play.api.libs.json.Reads._
import scala.concurrent._
/**
  * This class holds the model for a Survey.
  * The Kaggle data set has many more fields.
  * @param age
  * @param gender
  * @param income
  * @param hoursLearning
  */
case class Survey(age: Int, gender: String, income: Int, hoursLearning: Int)

/**
  * The companiant class holds all "static" methods and fields for a Survey
  * Eventually this data is replaced with persistant storage
  */
object Survey {
  val surveyList = List(
    Survey(28, "male", 62000, 10),
    Survey(30, "female", 23000, 20),
    Survey(23, "male", 42000, 25),
    Survey(45, "female", 32000, 32),
    Survey(34, "male", 12000, 27),
    Survey(56, "female", 45000, 10),
    Survey(34, "male", 32000, 5),
    Survey(28, "female", 25000, 11),
    Survey(28, "male", 22000, 13),
    Survey(28, "female", 22000, 7)
  )

  implicit val surveyWrites = Json.writes[Survey]

  implicit val surveyReads = Json.reads[Survey]

  def create(survey: Survey): List[Survey] = {
    survey :: surveyList
  }

  def fetch: List[Survey] = {
    surveyList
  }

  def fetchByGender(gender: String) = {
    surveyList.filter(s => s.gender.equals(gender)) //used to have contains - cuidado
  }

  def fetchByAge(age: Int): List[Survey] = {
    surveyList.filter(s => s.age == age)
  }

  def fetchByAgeRange(start: Int, end:Int): List[Survey] = {
    if (isStartLessOrEqualThanEnd(start, end)) {
      surveyList.filter(s => s.age >= start && s.age <= end)
    }
    else {
      List.empty[Survey]
    }
  }
  //helper methods
  def isStartLessOrEqualThanEnd(start: Int, end: Int): Boolean = {
    start match {
      case n => if (n > -1 && n <= end) true
        else false
      case _ => false
    }
  }
}