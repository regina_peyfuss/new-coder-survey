package controllers

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._
import play.api.test.Helpers._

import models._
/**
 * This will test helper methods for the Survey Model.
 * You can mock out a whole application including requests, plugins etc.
 *
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class SurveyUtilSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "SurveyModel helper method isStartLessOrEqualThanEnd" should {

    "check that start is greater than -1 " in {
      assert(Survey.isStartLessOrEqualThanEnd(-1, 3) == false)
      assert(Survey.isStartLessOrEqualThanEnd(1, 3) == true)
    }

    "check that start is less than end" in {
      assert(Survey.isStartLessOrEqualThanEnd(3, 2) == false)
      assert(Survey.isStartLessOrEqualThanEnd(1, 2) == true)
    }
  }
  "SurveyModel method fetch" should {

    "return a list of suveys with a size of 10" in {
      assert(Survey.fetch.size == 10)
    }
  }
  "SurveyModel method fetchByGender" should {

    "return a list of suveys with a size of 5 for males" in {
      assert(Survey.fetchByGender("male").size == 5)
    }
    "return a list of suveys with a size of 5 for females" in {
      assert(Survey.fetchByGender("female").size == 5)
    }
  }
  "SurveyModel method fetchByAge" should {

    "return a list of suveys with a size of 5 for age of 28 to 30" in {
      assert(Survey.fetchByAgeRange(28, 32).size == 5)
    }
  }
}
