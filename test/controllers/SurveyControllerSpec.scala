package controllers

import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._
import play.api.test.Helpers._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 *
 * For more information, see https://www.playframework.com/documentation/latest/ScalaTestingWithScalaTest
 */
class SurveyControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting {

  "SurveyController GET /api/surveys" should {

    "render content type application/json from a new instance of controller" in {
      val controller = new SurveyController(stubControllerComponents())
      val survey = controller.getSurveys().apply(FakeRequest(GET, "/api/surveys"))

      status(survey) mustBe OK
      contentType(survey) mustBe Some("application/json")
    }

    "render content type application/json from the application" in {
      val controller = inject[SurveyController]
      val survey = controller.getSurveys().apply(FakeRequest(GET, "/api/surveys"))

      status(survey) mustBe OK
      contentType(survey) mustBe Some("application/json")
      //contentAsString(home) must include ("Welcome to Play")
    }

    "render content type application/json from the router" in {
      val request = FakeRequest(GET, "/api/surveys")
      val survey = route(app, request).get

      status(survey) mustBe OK
      contentType(survey) mustBe Some("application/json")
      //contentAsString(home) must include ("Welcome to Play")
    }

    "SurveyController GET /api/surveys?gender=male" should {

      "render content type application/json from a new instance of controller" in {
        val controller = new SurveyController(stubControllerComponents())
        val survey = controller.getSurveysByGender("male").apply(FakeRequest(GET, "/api/surveys?gender=male"))

        status(survey) mustBe OK
        contentType(survey) mustBe Some("application/json")
      }
    }

    "SurveyController GET /api/surveys?age=28" should {

      "render content type application/json from a new instance of controller" in {
        val controller = new SurveyController(stubControllerComponents())
        val survey = controller.getSurveysByAge(28).apply(FakeRequest(GET, "/api/surveys?gender=male"))
        status(survey) mustBe OK
        contentType(survey) mustBe Some("application/json")
      }
    }
  }
}
